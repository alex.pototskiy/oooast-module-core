<?php
/**
 * PHP version 7.1
 * Import mode constants
 *
 * @category Export_Import_Tools
 * @package  CaMSoft\Core\CategoryCsv
 * @author   Alexander Pototskiy <alex@pototskiy.net>
 * @license  http://opensource.org/licenses/gpl-license.php GPL
 * @link     CaMSoft Import/Export tools
 * Date: 02.12.2018
 * Time: 8:43
 */

namespace OooAst\Core\CategoryCsv;

use Magento\Framework\Data\OptionSourceInterface;

/**
 * Class ImportMode
 *
 * @category Export_Import_Tools
 * @package  CaMSoft\Core\CategoryCsv
 * @author   Alexander Pototskiy <alex@pototskiy.net>
 * @license  http://opensource.org/licenses/gpl-license.php GPL
 * @link     CaMSoft Import/Export tools
 */
class ImportMode implements OptionSourceInterface
{
    /**
     * Add only new categories
     */
    const ADD = 1;
    /**
     * Add new and update existing category
     */
    const ADD_UPDATE = 2;
    /**
     * Update only existing category
     */
    const UPDATE = 3;

    private $_names = [];

    /**
     * ImportModeEnum constructor.
     */
    public function __construct()
    {
        $this->_names[self::ADD] = __('Add new');
        $this->_names[self::ADD_UPDATE] = __('Add and update');
        $this->_names[self::UPDATE] = __('Update existing');
    }


    /**
     * Prepare options for enum
     *
     * @inheritdoc
     *
     * @return array
     */
    public function toOptionArray(): array
    {
        $options = [];
        foreach ($this->_names as $key => $value) {
            $options[] = ['value' => $key, 'label' => $value];
        }
        return $options;
    }
}
