<?php
/**
 * PHP version 7.1
 * Image locations for import
 *
 * @category Import_Export_Tools
 * @package  CaMSoft\Core\CategoryCsv
 * @author   Alexander Pototskiy <alex@pototskiy.net>
 * @license  http://opensource.org/licenses/gpl-license.php GPL
 * @link     CaMSoft Import/Export tools
 * Date: 02.12.2018
 * Time: 19:01
 */

namespace OooAst\Core\CategoryCsv;

use Magento\Framework\Data\OptionSourceInterface;

/**
 * Class ImportImageLocation
 * Enum for import image location
 *
 * @category Import_Export_Tools
 * @package  CaMSoft\Core\CategoryCsv
 * @author   Alexander Pototskiy <alex@pototskiy.net>
 * @license  http://opensource.org/licenses/gpl-license.php GPL
 * @link     CaMSoft Import/Export tools
 */
class ImportImageLocation implements OptionSourceInterface
{
    /**
     * Images already in standard location
     */
    const IN_PLACE = 1;
    /**
     * Images in temporary location and should be copied.
     */
    const TMP_LOCATION = 2;

    private $_names = [];

    /**
     * ImportImageLocation constructor.
     */
    public function __construct()
    {
        $this->_names[self::IN_PLACE] = __('In place');
        $this->_names[self::TMP_LOCATION] = __('Temporary location');
    }

    /**
     * Prepare options for image location enum
     *
     * @inheritdoc
     *
     * @return array
     */
    public function toOptionArray(): array
    {
        $options = [];
        foreach ($this->_names as $key => $value) {
            $options[] = ['value' => $key, 'label' => $value];
        }
        return $options;
    }
}
