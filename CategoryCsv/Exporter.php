<?php
/**
 * PHP version 7.1
 * Category to CSV data exporter
 *
 * @category Export_Import_Tools
 * @package  CaMSoft\Core\CategoryCsv
 * @author   Alexander Pototskiy <alex@pototskiy.net>
 * @license  http://opensource.org/licenses/gpl-license.php GPL
 * @link     CaMSoft Import/Export tools
 * Date: 26.11.2018
 * Time: 7:34
 */

namespace OooAst\Core\CategoryCsv;

use Magento\Catalog\Api\Data\CategoryInterface;
use Magento\Catalog\Helper\Category as CategoryHelper;
use Magento\Catalog\Model\Category;
use Magento\Catalog\Model\CategoryRepository;
use Magento\Catalog\Model\ResourceModel\Category\Collection as CategoryCollection;
use Magento\Catalog\Model\ResourceModel\Category\CollectionFactory;
use Magento\Catalog\Model\ResourceModel\Product\Collection as ProductCollection;
use Magento\Eav\Model\Config;
use Magento\Framework\App\Filesystem\DirectoryList;
use Magento\Framework\App\ObjectManager;
use Magento\Framework\Data\Collection;
use Magento\Framework\Exception\FileSystemException;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\Filesystem;
use Magento\Store\Model\StoreManagerInterface;

/**
 * Class CategoryToCsv
 * Create CSV data file from store categories.
 *
 * @category Import_Export_Tools
 * @package  CaMSoft\Core\CategoryCsv
 * @author   Alexander Pototskiy <alex@pototskiy.net>
 * @license  http://opensource.org/licenses/gpl-license.php GPL
 * @link     CaMSoft Import/Export tools
 */
class Exporter
{
    const FILE_NAME = 'categories';
    const FILE_EXT = 'csv';
    const KEY_STORE_ID = 'gen_store_id';
    const KEY_STORE_CODE = 'gen_store_code';
    const KEY_CATEGORIES = 'gen_categories';
    const KEY_PRODUCTS = 'gen_products';

    const GEN_HEADERS = [
        self::KEY_STORE_ID,
        self::KEY_STORE_CODE,
        self::KEY_CATEGORIES,
        self::KEY_PRODUCTS
    ];

    /**
     * Object Manager
     *
     * @var ObjectManager
     */
    private $_objMan;
    /**
     * Store Manager
     *
     * @var StoreManagerInterface
     */
    private $_storeManager;
    /**
     * Category Helper
     *
     * @var CategoryHelper
     */
    private $_categoryHelper;
    /**
     * Category Repository
     *
     * @var CategoryRepository
     */
    private $_repo;
    /**
     * Category attributes
     *
     * @var array
     */
    private $_attributes;
    /**
     * Store id to work with
     *
     * @var int
     */
    private $_workStoreId;

    /**
     * CategoryToCsv constructor.
     *
     * @param StoreManagerInterface $storeManagement The store manager
     * @param CategoryHelper $categoryHelper The category helper
     * @param CategoryRepository $repo The category repository
     * @throws LocalizedException
     */
    public function __construct(
        StoreManagerInterface $storeManagement,
        CategoryHelper $categoryHelper,
        CategoryRepository $repo
    ) {
        $this->_objMan = ObjectManager::getInstance();
        $this->_storeManager = $storeManagement;
        $this->_categoryHelper = $categoryHelper;
        $this->_repo = $repo;

        $config = $this->_objMan->create(Config::class);
        $entity = $this->_objMan->create(CollectionFactory::class)->create();
        $defaultAttributes = $entity->getEntity()->getDefaultAttributes();
        $attributes = array_keys(
            $config->getEntityAttributes($entity->getEntity()->getType())
        );
        $this->_attributes = array_merge(
            array_diff($defaultAttributes, $attributes),
            $attributes
        );

    }

    /**
     * Read all categories from specified root and convert to CSV data string
     *
     * @param int|null $storeId The store id
     * @param int|null $startNodeId The root category id, null - use store root cat
     *
     * @return string file name with dsv data
     *
     * @throws NoSuchEntityException
     * @throws LocalizedException
     */
    public function exportToCsv(
        ?int $storeId = null,
        ?int $startNodeId = null
    ): string {
        try {
            $this->_workStoreId = $this->_storeManager->getStore()->getId();
            if (!is_null($storeId)) {
                $this->_workStoreId = $this->_storeManager
                    ->getStore($storeId)->getId();
            }
        } catch (NoSuchEntityException $e) {
            throw $e;
        }
        if (is_null($startNodeId)) {
            $startNodeId = 1;
        }
        /** @noinspection PhpArrayMapCanBeConvertedToLoopInspection */
        $csv
            = implode(
                ',',
                array_map(
                    function (string $v): string {
                        $escaped = str_replace('"', '""', $v);
                        return '"' . $escaped . '"';
                    },
                    array_merge($this->_attributes, self::GEN_HEADERS)
                )
            )
            . "\n";
        $cat = $this->_repo->get($startNodeId, $this->_workStoreId);
        $this->_addToCsv($cat, $csv);
        $fileName = $this->_writeFile(rtrim($csv, "\n"));
        return $fileName;
    }

    /**
     * Add category csv data to string, it calls itself to finish with child category
     *
     * @param CategoryInterface $cat The category to export
     * @param string $csv The data string to collect CSV data
     *
     * @return void
     * @throws LocalizedException
     */
    private function _addToCsv(CategoryInterface $cat, string &$csv): void
    {
        if ($cat instanceof Category) {
            $values = [];
            foreach ($this->_attributes as $item) {
                $value = $cat->getData($item);
                if (is_null($value)) {
                    $values[$item] = '';
                } else {
                    $escaped = str_replace('"', '""', $value);
                    $values[$item] = '"' . $escaped . '"';
                }
            }
            $values[self::KEY_STORE_ID] = '"' . $cat->getStore()->getId() . '"';
            $values[self::KEY_STORE_CODE] = '"' . $cat->getStore()->getCode() . '"';
            $values[self::KEY_CATEGORIES] = '"' . $this->_createCatPath($cat) . '"';
            $values[self::KEY_PRODUCTS] = '"' . $this->_createProductList($cat) . '"';
            $csv .= implode(',', $values) . "\n";
        }
        $collection = $this->_objMan->create(CategoryCollection::class)
            ->addFieldToFilter('parent_id', ['eq' => $cat->getId()])
            ->load()
            ->getAllIds();
        /**
         * The child category
         *
         * @var CategoryInterface $item
         */
        foreach ($collection as $id) {
            $item = $this->_repo->get($id, $this->_workStoreId);
            $this->_addToCsv($item, $csv);
        }
    }

    /**
     * Create category path with names
     *
     * @param CategoryInterface $cat The category to build path
     *
     * @return string
     * @throws LocalizedException
     */
    private function _createCatPath(CategoryInterface $cat): string
    {
        $ids = explode('/', $cat->getPath());
        $cats = $this->_objMan->create(CategoryCollection::class)
            ->addAttributeToSelect('name')
            ->addFieldToFilter('entity_id', ['in' => $ids])
            ->addOrder('entity_id', Collection::SORT_ORDER_ASC)
            ->load()
            ->getItems();

        $names = [];

        /**
         * Path category
         *
         * @var CategoryInterface $v
         */
        foreach ($cats as $v) {
            $names[] = $v->getName();
        }
        return implode('/', $names);
    }

    /**
     * Create product list of the category
     *
     * @param CategoryInterface $cat The category to find product
     *
     * @return string
     * @throws NoSuchEntityException
     */
    private function _createProductList(CategoryInterface $cat): string
    {
        if ($cat instanceof Category) {
            if ($cat->getId() == $cat->getStore()->getRootCategoryId()) {
                return '';
            }
            $prods = $this->_objMan->create(ProductCollection::class)
                ->addCategoryFilter($cat)
                ->getAllIds();

            $ids = [];
            foreach ($prods as $v) {
                $ids[] = $v;
            }
            return implode('|', $ids);
        } else {
            return '';
        }
    }

    /**
     * Write CSV data to file in var directory
     *
     * @param string $content The content to write to file
     *
     * @return string The wrote file name
     * @throws FileSystemException
     */
    private function _writeFile(string $content): string
    {
        $fs = $this->_objMan->create(Filesystem::class);
        $dir = $fs->getDirectoryWrite(DirectoryList::VAR_DIR);
        $fileName = self::FILE_NAME . '_' . date('Ymd_His') . '.' . self::FILE_EXT;
        $dir->writeFile($fileName, $content);
        return $fileName;
    }
}
