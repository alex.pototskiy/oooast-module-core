<?php
/**
 * PHP version 7.1
 * Extended store options, all store option added
 *
 * @category Import_Export_Tools
 * @package  CaMSoft\Core\CategoryCsv
 * @author   Alexander Pototskiy <alex@pototskiy.net>
 * @license  http://opensource.org/licenses/gpl-license.php GPL
 * @link     CaMSoft Import/Export tools
 * Date: 03.12.2018
 * Time: 18:33
 */

namespace OooAst\Core\CategoryCsv;

use Magento\Framework\App\ObjectManager;
use Magento\Framework\Data\OptionSourceInterface;
use Magento\Store\Ui\Component\Listing\Column\Store\Options;

/**
 * Class StoreOptions
 *
 * @category Import_Export_Tools
 * @package  CaMSoft\Core\CategoryCsv
 * @author   Alexander Pototskiy <alex@pototskiy.net>
 * @license  http://opensource.org/licenses/gpl-license.php GPL
 * @link     CaMSoft Import/Export tools
 */
class StoreOptions implements OptionSourceInterface
{

    /**
     * Magento store option
     *
     * @var Options
     */
    private $_storeOption;

    /**
     * StoreOptions constructor.
     */
    public function __construct()
    {
        $this->_storeOption = ObjectManager::getInstance()
            ->create(Options::class);
    }

    /**
     * Create store options with All Store option
     *
     * @inheritdocs
     *
     * @return array
     */
    public function toOptionArray(): array
    {
        return array_merge(
            [['value' => 0, 'label' => __('All Store Views')]],
            $this->_storeOption->toOptionArray()
        );
    }
}
