<?php
/**
 * PHP version 7.1
 * CSV data to category importer
 *
 * @package    CaMSoft
 * @subpackage Core
 * @author     Alexander Pototskiy <alex@pototskiy.net>
 * @copyright  2018-2018 CaMSoft Ltd
 * @license    http://opensource.org/licenses/gpl-license.php GPL
 * Date: 02.12.2018
 * Time: 8:35
 */

namespace OooAst\Core\CategoryCsv;

use Exception;
use Magento\Catalog\Api\Data\CategoryInterface;
use Magento\Catalog\Model\Category;
use Magento\Catalog\Model\CategoryFactory;
use Magento\Catalog\Model\CategoryRepository;
use Magento\Catalog\Model\ResourceModel\Category\CollectionFactory;
use Magento\Framework\App\Filesystem\DirectoryList;
use Magento\Framework\App\ObjectManager;
use Magento\Framework\Data\Collection;
use Magento\Framework\Exception\CouldNotSaveException;
use Magento\Framework\Exception\FileSystemException;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\File\Csv;
use Magento\Framework\Filesystem;
use Magento\Store\Model\StoreManagerInterface;

/**
 * Class Importer
 * Create system categories from CSV data file
 */
class Importer
{
    // Define import log keys.
    const READ_ITEMS_KEY = 'read';
    const PROCESSED_ITEMS_KEY = 'processed';
    const UPDATED_ITEMS_KEY = 'updated';
    const CREATED_ITEMS_KEY = 'created';
    const SKIPPED_ITEMS_KEY = 'skipped';
    const LOG_ITEMS_KEY = 'items';
    const ITEM_ACTION_KEY = 'action';
    const ITEM_ERROR_KEY = 'error';
    const COMMON_ERROR_KEY = 'error';
    // Define common constants.
    const IMPORT_DIR = 'category_import';
    const CAT_IMAGE_DIR = 'catalog/category';
    const CAT_KEY = 'category';
    const INT_PATH_KEY = 'int_path';
    const STR_PATH_KEY = 'str_path';
    // CSV Headers.
    const H_ENTITY_ID = 'entity_id';
    const H_PARENT_ID = 'parent_id';
    const H_STORE_ID = 'gen_store_id';
    const H_NAME = 'name';
    const H_PATH = 'path';
    const H_IMAGE = 'image';
    const H_IS_ACTIVE = 'is_active';
    const H_IS_ANCHOR = 'is_anchor';
    const H_INCLUDE_IN_MENU = 'include_in_menu';
    const H_META_TITLE = 'meta_title';
    const H_META_KEYWORDS = 'meta_keywords';
    const H_META_DESCRIPTION = 'meta_description';
    const H_DISPLAY_MODE = 'display_mode';
    const H_CUSTOM_USE_PARENT_SETTINGS = 'custom_use_parent_settings';
    const H_CUSTOM_APPLY_TO_PRODUCTS = 'custom_apply_to_products';
    const H_CUSTOM_DESIGN = 'custom_design';
    const H_CUSTOM_DESIGN_FROM = 'custom_design_from';
    const H_CUSTOM_DESIGN_TO = 'custom_design_to';
    const H_DEFAULT_SORT_BY = 'default_sort_by';
    const H_PAGE_LAYOUT = 'page_layout';
    const H_DESCRIPTION = 'description';
    const H_STORE_CODE = 'gen_store_code';
    const H_CATEGORIES = 'gen_categories';
    const H_PRODUCTS = 'gen_products';

    const H_CREATED_AT = 'created_at';
    const H_UPDATED_AT = 'updated_at';
    const H_CHILDREN = 'children';
    const H_CHILDREN_COUNT = 'children_count';
    const H_LEVEL = 'level';
    const H_POSITION = 'position';

    /**
     * Attributed that should be ignored
     *
     * @var array
     */
    private $doNotImportAttrs = [
        self::H_CATEGORIES,
        self::H_ENTITY_ID,
        self::H_PATH,
        self::H_PARENT_ID,
        self::H_STORE_ID,
        self::H_STORE_CODE,
        self::H_PRODUCTS,
        self::H_NAME,
        self::H_CREATED_AT,
        self::H_UPDATED_AT,
        self::H_CHILDREN,
        self::H_CHILDREN_COUNT,
        self::H_LEVEL,
        self::H_POSITION,
    ];
    /**
     * CSV headers index
     *
     * @var array[int]
     */
    private $hIdx = [];
    /**
     * File system object
     *
     * @var Filesystem
     */
    private $fileSystem;
    /**
     * Magento object manager
     *
     * @var ObjectManager
     */
    private $objectManager;
    /**
     * Category repository
     *
     * @var CategoryRepository
     */
    private $repo;
    /**
     * CSV data file path
     *
     * @var string
     */
    private $filePath;
    /**
     * Store id to import data
     *
     * @var int
     */
    private $storeId;
    /**
     * Import mode @see ImportMode
     *
     * @var integer
     */
    private $mode;
    /**
     * Image location type @see ImportImageLocation
     *
     * @var integer
     */
    private $imageLocation;
    /**
     * Array of existing category
     *
     * @var array
     */
    private $existing;
    /**
     * Import log
     *
     * @var array
     */
    private $importLog = [];
    /**
     * Magento store manager
     *
     * @var StoreManagerInterface
     */
    private $storeManager;
    /**
     * Csv file
     *
     * @var Csv
     */
    private $csv;
    /**
     * Category collection factory
     *
     * @var CollectionFactory
     */
    private $collectionFactory;
    /**
     * Magento category factory
     *
     * @var CategoryFactory
     */
    private $categoryFactory;

    /**
     * Importer constructor
     *
     * @param StoreManagerInterface $storeManager The store manager.
     * @param CategoryRepository $repo The category repository
     * @param Filesystem $fileSystem The file system object.
     * @param Csv $csv The CSV file
     * @param CollectionFactory $collectionFactory The category collection factory
     * @param CategoryFactory $categoryFactory The category factory
     */
    public function __construct(
        StoreManagerInterface $storeManager,
        CategoryRepository $repo,
        Filesystem $fileSystem,
        Csv $csv,
        CollectionFactory $collectionFactory,
        CategoryFactory $categoryFactory
    ) {
        $this->objectManager = ObjectManager::getInstance();
        $this->fileSystem = $fileSystem;
        $this->repo = $repo;
        $this->storeManager = $storeManager;
        $this->csv = $csv;
        $this->collectionFactory = $collectionFactory;
        $this->categoryFactory = $categoryFactory;
    }

    /**
     * Setup importer parameters
     *
     * @param string $filePath The CSV data file path, should be in VAR_DIR
     * @param int $storeId The destination store id
     * @param int $mode The import mode
     * @param int $imageLocation The import image file location
     *
     * @return void
     * @throws NoSuchEntityException
     */
    public function init(
        string $filePath,
        int $storeId,
        int $mode = ImportMode::ADD_UPDATE,
        int $imageLocation = ImportImageLocation::IN_PLACE
    ): void {
        $this->filePath = $filePath;
        $this->storeId = $storeId;
        $this->mode = $mode;
        $this->imageLocation = $imageLocation;
        $this->storeManager->setCurrentStore($this->storeManager->getStore($storeId)->getCode());
    }

    /**
     * Load categories from CSV file to DB
     *
     * @return array
     */
    public function importFromCsv(): array
    {
        $this->resetLog();
        try {
            $dir = $this->fileSystem->getDirectoryRead(DirectoryList::VAR_DIR);
            if (isset($this->filePath) && $dir->isExist($this->filePath)) {
                $this->existing = $this->getExistingCat($this->storeId);
                $data = $this->csv->getData($dir->getAbsolutePath($this->filePath));
                if (isset($data[0]) && !empty($data)) {
                    $this->getHeadersIndex(array_shift($data));
                    if (isset($this->hIdx[self::H_CATEGORIES])) {
                        $this->processDataItems($data);
                    } else {
                        throw new LocalizedException(__('Lack of data: no category path'));
                    }
                }
            } else {
                $this->importLog[self::COMMON_ERROR_KEY] = 'Data file does not exists.';
            }
        } catch (Exception $e) {
            $this->importLog[self::COMMON_ERROR_KEY] = $e->getMessage();
        }
        return $this->importLog;
    }

    /**
     * Clean log and prepare for new import
     *
     * @return void
     */
    private function resetLog(): void
    {
        $this->importLog = [];
        $this->importLog[self::READ_ITEMS_KEY] = 0;
        $this->importLog[self::PROCESSED_ITEMS_KEY] = 0;
        $this->importLog[self::CREATED_ITEMS_KEY] = 0;
        $this->importLog[self::UPDATED_ITEMS_KEY] = 0;
        $this->importLog[self::SKIPPED_ITEMS_KEY] = 0;
        $this->importLog[self::LOG_ITEMS_KEY] = [];
    }

    /**
     * Create array with info about existing categories
     *
     * @param int $storeId The store id
     *
     * @return array
     * @throws LocalizedException
     */
    private function getExistingCat(int $storeId): array
    {
        $v = [
            self::CAT_KEY => [],
            self::INT_PATH_KEY => [],
            self::STR_PATH_KEY => []
        ];
        $cats = $this->collectionFactory->create()
            ->setStoreId($storeId)
            ->addFieldToSelect('name')
            ->load()
            ->getItems();

        /**
         * Category
         *
         * @var CategoryInterface $cat
         */
        foreach ($cats as $cat) {
            $v[self::CAT_KEY][$cat->getId()] = $cat->getName();
            $v[self::INT_PATH_KEY][$cat->getId()] = $cat->getPath();
            $v[self::STR_PATH_KEY][$cat->getId()] = $this->createCatPath($cat);
        }
        return $v;
    }

    /**
     * Create category path with names
     *
     * @param CategoryInterface $cat The category to build path
     *
     * @return string
     * @throws LocalizedException
     */
    private function createCatPath(CategoryInterface $cat): string
    {
        $ids = explode('/', $cat->getPath());
        $cats = $this->collectionFactory->create()
            ->addAttributeToSelect('name')
            ->addFieldToFilter('entity_id', ['in' => $ids])
            ->addOrder('entity_id', Collection::SORT_ORDER_ASC)
            ->load()
            ->getItems();

        $names = [];

        /**
         * Path category
         *
         * @var CategoryInterface $v
         */
        foreach ($cats as $v) {
            $names[] = $v->getName();
        }
        return implode('/', $names);
    }

    /**
     * Prepare CSV headers index
     *
     * @param array[string] $data The headers array
     *
     * @return void
     */
    private function getHeadersIndex(array $data): void
    {
        foreach ($data as $key => $value) {
            $this->hIdx[$value] = $key;
        }
    }

    /**
     * Process csv data items
     *
     * @param array $data
     *
     * @throws FileSystemException
     */
    private function processDataItems(array $data): void
    {
        foreach ($data as $csvCat) {
            $this->importLog[self::READ_ITEMS_KEY]++;
            $catData = $this->getCatData($csvCat);
            if (isset($catData[self::H_ENTITY_ID])
                && $catData[self::H_ENTITY_ID] == 1
            ) {
                $this->addLogItem($csvCat[$this->hIdx[self::H_CATEGORIES]], 'skipped');
                $this->importLog[self::SKIPPED_ITEMS_KEY]++;
                $this->importLog[self::PROCESSED_ITEMS_KEY]++;
                continue;
            }
            $path = $csvCat[$this->hIdx[self::H_CATEGORIES]];
            $cat = $this->findExistingCat($path);
            if ($cat != null
                && ($this->mode == ImportMode::UPDATE
                    || $this->mode == ImportMode::ADD_UPDATE)
            ) {
                $this->processUpdate($path, $cat, $catData);
            } elseif ($cat == null
                && ($this->mode == ImportMode::ADD
                    || $this->mode == ImportMode::ADD_UPDATE)
            ) {
                $this->processCreate($path, $catData);
            }
        }
    }

    /**
     * Prepare category data from CSV
     *
     * @param array $csvCat The CSV data
     *
     * @return array  The category data in array format
     * @throws FileSystemException
     */
    private function getCatData(array $csvCat): array
    {
        $data = [];
        foreach ($this->hIdx as $header => $idx) {
            $v = $csvCat[$idx];
            if (isset($v) && !empty($v)) {
                if ($header == self::H_IMAGE) {
                    $v = $this->getImagePath($v);
                    if (isset($v) && !empty($v)) {
                        $data[$header] = $v;
                    }
                } elseif ($header == self::H_PRODUCTS) {
                    $v = explode('|', $v);
                    if (isset($v) && !empty($v)) {
                        $data[$header] = $v;
                    }
                } else {
                    $data[$header] = $v;
                }
            } else {
                $data[$header] = '';
            }
        }
        return $data;
    }

    /**
     * Prepare image path of category
     *
     * @param string $categoryImage The image location
     *
     * @return string
     * @throws FileSystemException
     */
    private function getImagePath(string $categoryImage): ?string
    {
        $url = preg_match('/^https?:\/\//', $categoryImage);
        if ($url) {
            return $categoryImage;
        }
        $allowed = ['gif', 'png', 'jpg', 'jpeg'];
        // @codingStandardsIgnoreLine
        $ext = strtolower(pathinfo($categoryImage, PATHINFO_EXTENSION));
        if (!in_array($ext, $allowed)) {
            return null;
        }
        $dir = $this->fileSystem->getDirectoryWrite(DirectoryList::MEDIA);
        $dir->create(self::CAT_IMAGE_DIR);
        if ($this->imageLocation == ImportImageLocation::IN_PLACE) {
            $filePath = $dir->getAbsolutePath(
                self::CAT_IMAGE_DIR . '/' . $categoryImage
            );
            // @codingStandardsIgnoreLine
            if (file_exists($filePath) && in_array($ext, $allowed)) {
                return $categoryImage;
            } else {
                return null;
            }
        } else {
            $imagePath = $this->fileSystem
                ->getDirectoryRead(DirectoryList::MEDIA)
                ->getAbsolutePath(self::CAT_IMAGE_DIR);
            // @codingStandardsIgnoreLine
            $file = file_get_contents($categoryImage);
            if ($file != '') {
                // @codingStandardsIgnoreLine
                $imageName = pathinfo($categoryImage, PATHINFO_BASENAME);
                // @codingStandardsIgnoreLine
                if (!is_dir($imagePath)) {
                    $dir->create(self::CAT_IMAGE_DIR . '/' . $imagePath);
                }
                $imagePath = $imagePath . '/' . $imageName;
                // @codingStandardsIgnoreLine
                $result = file_put_contents($imagePath, $file);
                if ($result) {
                    return $imageName;
                }
            }
        }
        return '';
    }

    /**
     * Add item log
     *
     * @param string $name The category name
     * @param string $action The executed action
     * @param string|null $error The action error
     *
     * @return void
     */
    private function addLogItem(
        string $name,
        string $action,
        ?string $error = null
    ): void {
        $this->importLog[self::LOG_ITEMS_KEY]
        [$name][self::ITEM_ACTION_KEY] = $action;
        if (!($error === null) && !empty($error)) {
            $this->importLog[self::LOG_ITEMS_KEY]
            [$name][self::ITEM_ERROR_KEY] = $error;
        }
    }

    /**
     * Find existing category base on path
     *
     * @param string $path The path of category
     *
     * @return CategoryInterface
     */
    private function findExistingCat(string $path): ?CategoryInterface
    {
        $id = array_search($path, $this->existing[self::STR_PATH_KEY]);
        try {
            if (isset($id)) {
                return $this->repo->get($id);
            }
        } catch (NoSuchEntityException $e) {
            return null;
        }
        return null;
    }

    /**
     * Process category update
     *
     * @param string $path The category path
     * @param CategoryInterface|null $cat The category
     * @param array $catData The new category data
     *
     * @return void
     */
    private function processUpdate($path, ?CategoryInterface $cat, array $catData): void
    {
        try {
            if ($this->updateCategory($cat, $catData)) {
                $this->addLogItem($path, 'updated');
                $this->importLog[self::UPDATED_ITEMS_KEY]++;
            } else {
                $this->addLogItem($path, 'skipped');
                $this->importLog[self::SKIPPED_ITEMS_KEY]++;
            }
            $this->importLog[self::PROCESSED_ITEMS_KEY]++;
        } catch (CouldNotSaveException $e) {
            $this->addLogItem($path, 'update error', $e->getMessage());
        }
    }

    /**
     * Update category
     *
     * @param CategoryInterface $cat The category to update
     * @param array $catData The data from CSV
     *
     * @return bool True - updated, False - nothing to update
     * @throws CouldNotSaveException
     */
    private function updateCategory(CategoryInterface $cat, array $catData): bool
    {
        $needToSave = false;
        if ($cat instanceof Category) {
            foreach ($this->doNotImportAttrs as $header) {
                unset($catData[$header]);
            }

            foreach ($catData as $key => $value) {
                if ($cat->getData($key) != $value) {
                    $cat->setData($key, $value);
                    $needToSave = true;
                }
            }
            if ($needToSave) {
                $this->repo->save($cat);
            }
        }
        return $needToSave;
    }

    /**
     * Process create category
     *
     * @param string $path The category path
     * @param array $catData The category data
     *
     * @return void
     */
    private function processCreate($path, array $catData): void
    {
        try {
            $cat = $this->_createCategory($catData, $this->storeId);
            $this->updateCategory($cat, $catData);
            $this->addLogItem($path, 'created');
            $this->importLog[self::CREATED_ITEMS_KEY]++;
            $this->importLog[self::PROCESSED_ITEMS_KEY]++;
        } catch (Exception $e) {
            $this->addLogItem($path, 'create error', $e->getMessage());
        }
    }

    /**
     * Create new category with minimum data
     *
     * @param array $catData The CSV data to create category
     * @param int $storeId The destination store id
     *
     * @return CategoryInterface|null
     * @throws CouldNotSaveException
     * @throws LocalizedException
     * @throws Exception
     */
    private function _createCategory(
        array $catData,
        int $storeId
    ): ?CategoryInterface {
        // @codingStandardsIgnoreLine
        $parents = pathinfo($catData[self::H_CATEGORIES], PATHINFO_DIRNAME);
        $cat = $this->findExistingCat($parents);
        if ($cat == null) {
            $m = __('Parents does not exist for category: ' . $catData[self::H_CATEGORIES]);
            throw new LocalizedException($m);
        }
        $newCat = $this->categoryFactory->create();
        $newCat->setStoreId($storeId);
        $newCat->setParentId($cat->getId());
        $newCat->setName($catData[self::H_NAME]);
        $newCat->setIsActive(false);
        $newCat = $this->repo->save($newCat);
        $this->existing[self::CAT_KEY][$newCat->getId()] = $newCat->getName();
        $this->existing[self::INT_PATH_KEY][$newCat->getId()] = $newCat->getPath();
        $this->existing[self::STR_PATH_KEY][$newCat->getId()]
            = $this->createCatPath($newCat);
        return $newCat;
    }
}
